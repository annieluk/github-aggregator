<?php

namespace app\services;

use app\models\GithubRepo;
use yii\db\Exception;

class RepoUpdateService
{
    /**
     * @param GithubRepo[] $repoList
     * @return void
     * @throws
     */
    public function updateRepoList(array $repoList): void
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if (!$repoList) {
                GithubRepo::deleteAll();
            } else {
                $oldRepoNames = GithubRepo::find()->select('full_name')->column();
                $actualRepoNames = [];

                foreach ($repoList as $newRepo) {
                    if (in_array($newRepo->full_name, $oldRepoNames)) {
                        $actualRepoNames[] = $newRepo->full_name;
                    } else {
                        $newRepo->save();
                    }
                }

                GithubRepo::deleteAll(['full_name' => array_diff($oldRepoNames, $actualRepoNames)]);
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new Exception($e->getMessage());
        }

        $transaction->commit();
    }
}