<?php

namespace app\services;

use app\models\GithubRepo;

class RepoFilterService
{
    /**
     * Sorts array from newest to oldest and receives 10 newest repo
     *
     * @param string[] $usersRepoList
     * @return GithubRepo[]
     */
    public function getLastReposForInsert(array $usersRepoList): array
    {
        arsort($usersRepoList);
        $filteredArray = array_slice($usersRepoList, 0, 10);

        return array_map(
            fn($time, $name) => new GithubRepo(
                ['updated_at' => $time, 'full_name' => $name]
            ),
            $filteredArray,
            array_keys($filteredArray)
        );
    }
}