<?php

use app\models\GithubRepo;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Github Repos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="github-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => yii\grid\ActionColumn::class,
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, GithubRepo $model)  {
                        $repoName = Html::encode($model->full_name);
                        return Html::a($repoName, "https://github.com/$repoName", [
                            'class' => 'btn btn-success',
                        ]);
                    }
                ],
            ]
        ]
    ]); ?>

</div>
