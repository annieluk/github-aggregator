<?php

namespace app\services;

use linslin\yii2\curl\Curl;

class GithubApiService
{
    /** @var string */
    private const GITHUB_BASE_URL = 'https://api.github.com/';

    /** @var string[] */
    private const CURL_OPTIONS = [CURLOPT_RETURNTRANSFER => true, CURLOPT_USERAGENT => 'PostmanRuntime/7.26.10'];

    /** @var string */
    private const TOKEN_VAR_NAME = 'PERSONAL_GITHUB_TOKEN';

    /**
     * Receives data about all user's repos
     *
     * @param string[] $userNames
     * @return int[]
     */
    public function getUsersRepoList(array $userNames): array
    {
        $repoList = [];

        foreach ($userNames as $userName) {
            $repoList += $this->getUserRepoList($userName);
        }

        return $repoList;
    }

    /**
     * Receives and filter data about public user repos
     *
     * @param string $userName
     * @return int[]
     * @throws
     */
    private function getUserRepoList(string $userName): array
    {
        $options = self::CURL_OPTIONS;

        if (getenv(self::TOKEN_VAR_NAME)) {
            $options += [CURLOPT_HTTPHEADER => ['Authorization: Bearer ' . getenv(self::TOKEN_VAR_NAME)]];
        }

        $repoData = [];
        $curl = new Curl();

        $response = $curl
            ->setOptions($options)
            ->get(self::GITHUB_BASE_URL . 'users/' . $userName . '/repos', false);

        foreach ($response as $repoSettings) {
            if (isset($repoSettings['full_name'])) {
                $repoData[$repoSettings['full_name']] = $repoSettings['updated_at'];
            }
        }

        return $repoData;
    }
}