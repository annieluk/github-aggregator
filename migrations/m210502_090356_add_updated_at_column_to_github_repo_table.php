<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%github_repo}}`.
 */
class m210502_090356_add_updated_at_column_to_github_repo_table extends Migration
{
    private string $tableName = 'github_repo';
    private string $columnName = 'updated_at';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, $this->columnName, $this->timestamp());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, $this->columnName);
    }
}
