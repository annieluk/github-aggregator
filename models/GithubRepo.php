<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "github_repo".
 *
 * @property int $id
 * @property string|null $full_name
 * @property string $updated_at
 */
class GithubRepo extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'github_repo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['full_name'], 'string', 'max' => 255],
            [['full_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Full Name',
        ];
    }
}