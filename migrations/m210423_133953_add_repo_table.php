<?php

use yii\db\Migration;

/**
 * Class m210423_133953_add_repo_table
 */
class m210423_133953_add_repo_table extends Migration
{
    private string $tableName = 'github_repo';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
            'full_name' => $this->string()->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
