<?php

namespace app\commands;

use app\repositories\GithubUserRepository;
use app\services\GithubApiService;
use app\services\RepoFilterService;
use app\services\RepoUpdateService;

class UpdateRepoListCommand
{
    private GithubApiService $githubApiService;
    private RepoFilterService $repoFilterService;
    private GithubUserRepository $githubUserRepository;
    private RepoUpdateService $repoUpdateService;

    public function __construct()
    {
        $this->githubApiService = \Yii::createObject(GithubApiService::class);
        $this->repoFilterService = \Yii::createObject(RepoFilterService::class);
        $this->githubUserRepository = \Yii::createObject(GithubUserRepository::class);
        $this->repoUpdateService = \Yii::createObject(RepoUpdateService::class);
    }

    /**
     * @throws
     */
    public function execute(): void
    {
        $users = $this->githubUserRepository->getAllUserNames();
        $usersRepoList = $this->githubApiService->getUsersRepoList($users);

        $filteredRepoList = $usersRepoList ?
            $this->repoFilterService->getLastReposForInsert($usersRepoList)
            : [];

        $this->repoUpdateService->updateRepoList($filteredRepoList);
    }
}