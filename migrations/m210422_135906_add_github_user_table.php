<?php

use yii\db\Migration;

/**
 * Class m210422_135906_add_github_user_table
 */
class m210422_135906_add_github_user_table extends Migration
{
    private string $tableName = 'github_user';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
                'id' => $this->bigPrimaryKey(),
                'user_name' => $this->string()->unique(),
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
