<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "github_user".
 *
 * @property int $id
 * @property string|null $user_name
 */
class GithubUser extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'github_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_name'], 'string', 'max' => 255],
            [['user_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_name' => 'User Name',
        ];
    }
}
