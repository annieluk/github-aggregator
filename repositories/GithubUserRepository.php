<?php

namespace app\repositories;

use yii\db\Query;

class GithubUserRepository
{
    /**
     * @return string[]
     */
    public function getAllUserNames(): array
    {
        return (new Query())
            ->select('user_name')
            ->from('github_user')
            ->column();
    }
}