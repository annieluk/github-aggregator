#!/bin/bash
composer install --prefer-dist
mkdir -p /app/runtime/cache
chmod ugo+w /app/web/assets
echo yes | php yii migrate
service cron start
apache2-foreground