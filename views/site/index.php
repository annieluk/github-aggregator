<?php

/* @var $this yii\web\View */

$this->title = 'GitHub Repo Receiver';
?>
<div class="site-index">

    <div class="jumbotron">
        <p><a class="btn btn-lg btn-success" href="/username">GitHub users</a></p>
    </div>

    <div class="jumbotron">
        <p><a class="btn btn-lg btn-success" href="/github">Last updated repos</a></p>
    </div>

</div>
