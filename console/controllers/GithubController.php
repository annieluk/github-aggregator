<?php

namespace app\console\controllers;

use app\commands\UpdateRepoListCommand;
use yii\console\Controller;

class GithubController extends Controller
{
    private UpdateRepoListCommand $updateRepoListCommand;

    public function __construct($id, $module, $config = [])
    {
        $this->updateRepoListCommand = \Yii::createObject(UpdateRepoListCommand::class);
        parent::__construct($id, $module, $config);
    }

    public function actionUpdate(): void
    {
        try {
            $this->updateRepoListCommand->execute();
        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
            echo $e->getTraceAsString() . PHP_EOL;
        }
    }
}